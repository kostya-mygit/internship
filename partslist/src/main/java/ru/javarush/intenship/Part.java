package ru.javarush.intenship;

import javax.persistence.*;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "PART")
@NamedQueries({
        @NamedQuery(name = "Part.findNeeded", query = "select p from Part p where p.need=true"),
        @NamedQuery(name = "Part.findOptional", query = "select p from Part p where p.need=false"),
        @NamedQuery(name = "Part.findAllByNameLike", query = "select p from Part p where  lower(p.name) like lower(concat('',:partName,'%'))"),
        @NamedQuery(name = "Part.findMinCountNeeded", query = "select min(p.count) from Part p where p.need=true")
})
public class Part implements Serializable {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NEED")
    private boolean need;

    @Column(name = "COUNT")
    private int count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNeed() {
        return need;
    }

    public void setNeed(boolean need) {
        this.need = need;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
