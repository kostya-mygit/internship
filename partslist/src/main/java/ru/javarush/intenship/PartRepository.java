package ru.javarush.intenship;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface PartRepository extends PagingAndSortingRepository<Part, Long> {
    Page<Part> findNeeded(Pageable pageRequest);
    Page<Part> findOptional(Pageable pageRequest);
    Part findById(Long id);
    Page<Part> findAllByNameLike(@Param("partName") String partName, Pageable pageRequest);
    Integer findMinCountNeeded();
}
