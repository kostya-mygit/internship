package ru.javarush.intenship;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PartService {
    Page<Part> findAll(Pageable pageRequest);
    Page<Part> findNeeded(Pageable pageRequest);
    Page<Part> findOptional(Pageable pageRequest);
    Part findById(Long id);
    Page<Part> findAllByNameLike(String partName, Pageable pageRequest);
    Integer findMinCountNeeded();
    Part save(Part part);
    void delete(Long id);
}
