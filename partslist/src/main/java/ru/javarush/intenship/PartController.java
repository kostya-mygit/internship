package ru.javarush.intenship;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/parts")
public class PartController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartController.class);

    @Autowired
    private PartService partService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String list(@RequestParam(value = "pageNum", required=false, defaultValue = "1") int pageNum,
                       @RequestParam(value = "listMode", required=false, defaultValue = "all") String listMode,
                       @RequestParam(value = "partName", required=false, defaultValue = "") String partName,
                       Model model) {

        PageRequest pageRequest = new PageRequest(pageNum - 1,10);
        Page<Part> page = null;
        if (listMode.equals("all")) {
            page = partService.findAll(pageRequest);
        }
        else if (listMode.equals("needed")) {
            page = partService.findNeeded(pageRequest);
        }
        else if (listMode.equals("optional")) {
            page = partService.findOptional(pageRequest);
        }
        else if (listMode.equals("found")) {
            page = partService.findAllByNameLike(partName, pageRequest);
        }
        model.addAttribute("page", page);
        model.addAttribute("listMode", listMode);
        model.addAttribute("partName", partName);
        model.addAttribute("minCountNeededParts", partService.findMinCountNeeded());
        return "parts/list";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String showAddForm(Model model){
        model.addAttribute("command", new Part());
        return "parts/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@ModelAttribute Part part) {
        partService.save(part);
        return "redirect:/";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String showEditForm(@PathVariable Long id, Model model){
        model.addAttribute("command", partService.findById(id));
        return "parts/edit";
    }

    @RequestMapping(value = "/edit/save", method = RequestMethod.POST)
    public String edit(@ModelAttribute Part part){
        partService.save(part);
        return "redirect:/";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable Long id) {
        partService.delete(id);
        return "redirect:/";
    }

    @ExceptionHandler(Exception.class)
    public String handleException(Exception exception, Model model) {
        LOGGER.error("ErrorLog: ", exception);
        model.addAttribute("exceptionClass", exception.getClass().getSimpleName());
        model.addAttribute("exceptionMessage", exception.getMessage());
        return "error/exception";
    }
}
