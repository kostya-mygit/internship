package ru.javarush.intenship;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("partService")
@Repository
@Transactional
public class PartServiceImpl implements PartService {

    @Autowired
    private PartRepository partRepository;

    @Transactional(readOnly = true)
    @Override
    public Page<Part> findAll(Pageable pageRequest) {
        return partRepository.findAll(pageRequest);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Part> findNeeded(Pageable pageRequest) {
        return partRepository.findNeeded(pageRequest);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Part> findOptional(Pageable pageRequest) {
        return partRepository.findOptional(pageRequest);
    }

    @Transactional(readOnly = true)
    @Override
    public Part findById(Long id) {
        return partRepository.findById(id);
    }

    @Override
    public Page<Part> findAllByNameLike(String partName, Pageable pageRequest) {
        return partRepository.findAllByNameLike(partName, pageRequest);
    }

    @Transactional(readOnly = true)
    @Override
    public Integer findMinCountNeeded() {
        Integer minCountNeeded = partRepository.findMinCountNeeded();
        if (minCountNeeded == null) return 0;
        return minCountNeeded;
    }

    @Override
    public Part save(Part part) {
        return partRepository.save(part);
    }

    @Override
    public void delete(Long id) {
        partRepository.delete(id);
    }
}
